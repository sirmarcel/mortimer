from unittest import TestCase
import numpy as np

from cmlkit import Dataset
from cmlkit.representation.composed import Composed
from cmlkit.representation.mbtr import MBTR1

from mortimer.reptimer import RepTimer


class TestComposed(TestCase):
    def setUp(self):
        self.data = Dataset(z=np.array([[0, 1]]), r=np.array([[[1, 2, 3], [1, 2, 3]]]))
        self.timer = RepTimer(self.data, repeats=10)

    def test_mbtr_1(self):
        mbtr = MBTR1(
            start=0,
            stop=4,
            num=5,
            geomf="count",
            weightf="unity",
            broadening=0.001,
            eindexf="noreversals",
            aindexf="noreversals",
            elems=[0, 1, 2, 3],
            flatten=True,
        )

        composed = Composed(mbtr, mbtr.get_config()).get_config()

        res, timings = self.timer.evaluate_rep(composed)
        res, timings_single = self.timer.evaluate_rep(mbtr.get_config())

        # this is basically a pointless assertion, we're
        # just making sure it does something (and it's a sanity check)
        self.assertLess(timings_single["run"]["mean"], timings["run"]["mean"])
