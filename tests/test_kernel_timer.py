from unittest import TestCase
import numpy as np

import cmlkit

from mortimer.kerneltimer import KernelTimer


class TestComposed(TestCase):
    def setUp(self):
        self.n = 1000
        self.n_atoms = np.random.randint(1, high=10, size=self.n)

        r = [2 * np.random.random((na, 3)) for na in self.n_atoms]
        self.r = np.array(r, dtype=object)
        self.z = np.array(
            [np.random.randint(1, high=3, size=na) for na in self.n_atoms], dtype=object
        )

        self.data = cmlkit.Dataset(z=self.z, r=self.r)
        self.timer = KernelTimer(self.data, repeats=10)

    def test_kerneltimer(self):

        model = {
            "model": {
                "regression": {
                    "krr": {
                        "nl": 1.0e-7,
                        "kernel": {
                            "kernel_global": {"kernelf": {"gaussian": {"ls": 1.0}}}
                        },
                    }
                },
                "representation": {
                    "mbtr_1": {
                        "start": 0.0,
                        "stop": 10,
                        "num": 100,
                        "geomf": "count",
                        "weightf": "unity",
                        "broadening": 0.001,
                        "elems": [0, 1, 2, 3],
                    }
                },
            }
        }

        res = self.timer(model)

        # just assert that it didn't fail
        self.assertLess(res["timings"]["mean"], 1.0)

        model = cmlkit.from_config(model)
        rep = model.representation(self.data)

        def f():
            return model.regression.kernel(rep)

        other_res, other_timings = cmlkit.utility.time_repeat(f, repeats=10)

        np.testing.assert_array_almost_equal(res["results"], other_res[0])
        self.assertLess(np.abs(res["timings"]["mean"]-other_timings["mean"]), 0.2*res["timings"]["mean"])
