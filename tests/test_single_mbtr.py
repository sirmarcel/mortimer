from unittest import TestCase
import numpy as np

from cmlkit import Dataset, from_config
from cmlkit.representation.mbtr import MBTR1, MBTR2, MBTR3
from cmlkit.utility import time_repeat

from mortimer.reptimer import RepTimer


class TestMBTR1(TestCase):
    def setUp(self):
        self.data = Dataset(z=np.array([[0, 1]]), r=np.array([[[1, 2, 3], [1, 2, 3]]]))
        self.timer = RepTimer(self.data, repeats=10)

    def test_mbtr_1(self):
        mbtr = MBTR1(
            start=0,
            stop=4,
            num=5,
            geomf="count",
            weightf="unity",
            broadening=0.001,
            eindexf="noreversals",
            aindexf="noreversals",
            elems=[0, 1, 2, 3],
            flatten=True,
        ).get_config()

        res, timings = self.timer.evaluate_rep(mbtr)

        # this is basically a pointless assertion, we're
        # just making sure it does something
        self.assertLess(timings["run"]["mean"], 1.0)

        m = from_config(mbtr)
        other_res, other_timings = time_repeat(lambda: m(self.data), repeats=10)

        np.testing.assert_array_almost_equal(res, other_res[0])
        self.assertLess(
            np.abs(other_timings["mean"] - timings["run"]["mean"]),
            0.5 * timings["run"]["mean"],
        )


class TestMBTR2(TestCase):
    def setUp(self):
        self.data = Dataset(
            z=np.array([[0, 0]]),
            r=np.array([[[0.0, 0.0, 0.0], [2.0, 0.0, 0.0]]]),
            b=np.array([[[1, 0, 0], [0, 1, 0], [0, 0, 1]]]),
        )
        self.timer = RepTimer(self.data, repeats=10)

    def test_mbtr_2(self):
        mbtr = MBTR2(
            start=0,
            stop=1,
            num=3,
            geomf="1/distance",
            weightf="identity",
            broadening=0.001,
            eindexf="noreversals",
            aindexf="noreversals",
            elems=[0],
            flatten=True,
        ).get_config()

        res, timings = self.timer.evaluate_rep(mbtr)

        # this is basically a pointless assertion, we're
        # just making sure it does something
        self.assertLess(timings["run"]["mean"], 1.0)

        m = from_config(mbtr)
        other_res, other_timings = time_repeat(lambda: m(self.data), repeats=10)

        np.testing.assert_array_almost_equal(res, other_res[0])
        self.assertLess(
            np.abs(other_timings["mean"] - timings["run"]["mean"]),
            0.10 * timings["run"]["mean"],
        )


class TestMBTR3(TestCase):
    def setUp(self):
        self.data = Dataset(
            z=np.array([[0, 0, 0]]),
            r=np.array([[[0.0, 0.0, 0.0], [0.1, 0.0, 0.0], [0.0, 0.2, 0.0]]]),
            b=np.array([[[1, 0, 0], [0, 1, 0], [0, 0, 1]]]),
        )
        self.timer = RepTimer(self.data, repeats=10)

    def test_mbtr_3(self):
        mbtr = MBTR3(
            start=0,
            stop=np.pi / 2 + 0.01,
            num=3,
            geomf="angle",
            weightf="1/normnormnorm",
            broadening=0.01,
            eindexf="noreversals",
            aindexf="noreversals",
            elems=[0],
            flatten=True,
        ).get_config()

        res, timings = self.timer.evaluate_rep(mbtr)

        # this is basically a pointless assertion, we're
        # just making sure it does something
        self.assertLess(timings["run"]["mean"], 1.0)

        m = from_config(mbtr)
        other_res, other_timings = time_repeat(lambda: m(self.data), repeats=10)

        np.testing.assert_array_almost_equal(res, other_res[0])
        self.assertLess(
            np.abs(other_timings["mean"] - timings["run"]["mean"]),
            0.10 * timings["run"]["mean"],
        )
