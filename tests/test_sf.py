import numpy as np
from unittest import TestCase
import unittest.mock
import shutil
import pathlib

from mortimer.reptimer import RepTimer

import cmlkit


class TestSymmetryFunctions(TestCase):
    def setUp(self):
        self.tmpdir = (pathlib.Path(__file__) / "..").resolve() / "tmp_test_sf"
        self.tmpdir.mkdir(exist_ok=True)

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def test_parametrized_sf(self):

        with unittest.mock.patch.dict("os.environ", {"CML_SCRATCH": str(self.tmpdir)}):
            from cmlkit.representation.sf import SymmetryFunctions
            from cmlkit import Dataset, runner_path

            print(runner_path)

            data = Dataset(
                z=np.array([[1, 1]]), r=np.array([[[0.0, 0.0, 0.0], [2.0, 0.0, 0.0]]])
            )

            timer = RepTimer(data, repeats=10)

            sf = SymmetryFunctions(
                [1], universal=[{"rad_centered": {"n": 3, "cutoff": 5.0}}]
            ).get_config()

            res, timings = timer.evaluate_rep(sf)

            print(timings)

            # this is basically a pointless assertion, we're
            # just making sure it does something
            self.assertLess(timings["run"]["mean"], 1.0)
            self.assertLess(timings["prep"]["mean"], 1.0)
            self.assertLess(timings["read"]["mean"], 1.0)

            total = timings["run"]["mean"] + timings["prep"]["mean"] + timings["read"]["mean"]

            sf = cmlkit.from_config(sf)

            def f():
                return sf(data)

            other_res, other_timings = cmlkit.utility.time_repeat(f, repeats=10)

            # sanity check
            np.testing.assert_array_almost_equal(res, other_res[0])
            self.assertLess(np.abs(total - other_timings["mean"]), 0.5*total)
