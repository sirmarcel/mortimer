from cmlkit import load_dataset, from_config, runner_path
from cmlkit.evaluation.evaluator import Evaluator
from cmlkit.engine import to_config, parse_config
from cmlkit.utility.timing import time_repeat

from cpuinfo import get_cpu_info


class KernelTimer(Evaluator):
    """An evaluator that times kernel computations.

    Given a config for a Model that contains a representation
    and a KRR regressor, this evaluator will first compute the
    representation, and then time the computation of the square
    kernel matrix between all systems in a dataset.

    Parameters:
        data: Dataset name, on which we will time
        repeats: Number of repeats

    Will return a dict with the keys:
        result: actually computed kernel matrix
        timings: output of `cmlkit.utility.timing.time_repeat`, i.e.
            dict with keys "times", "mean", "max", "min".
        cpu_info: Dict with CPU and platform information

    """

    kind = "eval_kerneltimer"

    def __init__(self, data, repeats=3, context={}):
        super().__init__(context=context)

        self.data = load_dataset(data)
        self.repeats = repeats

        self.cpu_info = get_cpu_info()

    def _get_config(self):
        return {"data": self.data.name, "repeats": self.repeats}

    def evaluate(self, model):
        model = to_config(model)
        rep = from_config(model["model"]["representation"])(self.data)

        kernel = from_config(model["model"]["regression"]["krr"]["kernel"])

        def run():
            # we compute the square kernel matrix, without
            # making use of symmetry -- so we have some repeats
            # already built in!
            return kernel(x=rep, z=rep)

        results, timings = time_repeat(run, repeats=self.repeats)

        return {"results": results[0], "timings": timings, "cpu_info": self.cpu_info}
