import shutil
from cmlkit import load_dataset, from_config, runner_path
from cmlkit.evaluation.evaluator import Evaluator
from cmlkit.engine import to_config, parse_config
from cmlkit.utility.timing import time_repeat
from cmlkit.env import get_plugins

from cmlkit.representation.sf.runner_input import prepare_task as sf_prepare_task
from cmlkit.representation.sf.runner_output import run_task as sf_run_task
from cmlkit.representation.sf.runner_output import read_result as sf_read_result
from cmlkit.representation.sf.runner import check_dependencies

from cmlkit.representation.soap.quippy_interface import make_quippy_config
from cmlkit.representation.soap.quippy_interface import prepare_task as soap_prepare_task
from cmlkit.representation.soap.quippy_interface import run_task as soap_run_task
from cmlkit.representation.soap.quippy_interface import read_result as soap_read_result

from cpuinfo import get_cpu_info


class RepTimer(Evaluator):
    """Evaluator for detailed timings of representations.

    Parameters:
        data: Dataset name, on which we will time
        repeats: Number of repeats


    Will return a dict with the keys:
        result: actually computed rep
        timings: dict of timings, with keys:
            "run": actual computation time,
            optionally: "prep" and "read" for pre/post-processing

            each of these keys points to another dict with the actual timings
            these are the output of `cmlkit.utility.timing.time_repeat`, i.e.
            dicts with keys "times", "mean", "max", "min".
        cpu_info: Dict with CPU and platform information

    This class is extremely hacky and fragile, which is why it's externalised
    in some plugin where it can't do too much damage.

    The reason it is hacky is because it essentially reimplements the
    "business part" of the representation classes in order to separate out
    the pre- and post-processing steps.

    """

    kind = "eval_reptimer"

    def __init__(self, data, repeats=3, context={}):
        super().__init__(context=context)

        self.data = load_dataset(data)
        self.repeats = repeats

        self.cpu_info = get_cpu_info()

    def _get_config(self):
        return {"data": self.data.name, "repeats": self.repeats}

    def evaluate(self, model):
        model = to_config(model)

        result, timings = self.evaluate_rep(model["model"]["representation"])

        return {"timings": timings, "result": result, "cpu_info": self.cpu_info}

    def evaluate_rep(self, representation):
        rep, inner = parse_config(representation)

        if rep == "composed":
            return self.time_composed(rep, inner)
        elif "mbtr" in rep:
            return self.time_mbtr(rep, inner)
        elif rep == "soap":
            return self.time_soap(rep, inner)
        elif rep == "sf":
            return self.time_sf(rep, inner)
        elif rep in ["ds_soap", "ds_sf", "ds_mbtr", "ds_lmbtr"]:
            return self.time_cscribe(rep, inner)
        else:
            raise ValueError(f"RepTimer currently can't time representation {rep}.")

    def time_composed(self, kind, inner):
        reps = [parse_config(rep) for rep in inner["reps"]]
        for kind, _ in reps:
            assert (
                "mbtr" in kind
            ), f"RepTimer can currently only time composed reps of MBTRs."

        instantiated_reps = [
            from_config(rep, context={"cache": None}) for rep in inner["reps"]
        ]

        def run():
            return [rep(self.data) for rep in instantiated_reps]

        results, run_timings = time_repeat(run, repeats=self.repeats)

        return results[0], {"run": run_timings}

    def time_mbtr(self, kind, inner):
        mbtr = from_config({kind: inner}, context={"cache": None})

        def run():
            return mbtr(self.data)

        results, run_timings = time_repeat(run, repeats=self.repeats)

        return results[0], {"run": run_timings}

    def time_cscribe(self, kind, inner):
        rep = from_config({kind: inner}, context={"cache": None})

        def run():
            return rep(self.data)

        results, run_timings = time_repeat(run, repeats=self.repeats)

        return results[0], {"run": run_timings}

    def time_sf(self, kind, inner):
        sf = from_config({kind: inner}, context={"cache": None})
        runner_config = sf.runner_config

        return timed_symmfs(self.repeats, self.data, runner_config)

    def time_soap(self, kind, inner):
        soap = from_config({kind: inner}, context={"cache": None})
        quippy_config = soap.quippy_config

        return timed_soap(self.repeats, self.data, quippy_config)


def timed_symmfs(repeats, data, config, timeout=None, cleanup=True):
    """Compute atom-centered symmetry functions with timings.

        See implementation in cmlkit for args etc.
    """

    check_dependencies()

    def timed_prep():
        return sf_prepare_task(data, config)

    folder, prep_timings = time_repeat(timed_prep, repeats=repeats)

    folder = folder[0]  # we actually need a usable result here

    def timed_run():
        out, err = sf_run_task(folder, timeout=timeout)
        return None

    res, run_timings = time_repeat(timed_run, repeats=repeats)

    def timed_read():
        return sf_read_result(data, folder, config)

    descriptors, read_timings = time_repeat(timed_read, repeats=repeats)

    if cleanup:
        shutil.rmtree(folder)

    return (
        descriptors[0],
        {"run": run_timings, "read": read_timings, "prep": prep_timings},
    )


def timed_soap(repeats, data, config, cleanup=True, timeout=None):
    """Compute SOAP with timings."""

    def timed_prep():
        quippy_config = make_quippy_config(config)
        return soap_prepare_task(data, quippy_config)

    folder, prep_timings = time_repeat(timed_prep, repeats=repeats)
    folder = folder[0]

    def timed_run():
        return soap_run_task(folder, timeout=timeout)

    res, run_timings = time_repeat(timed_run, repeats=repeats)

    def timed_read():
        return soap_read_result(folder)

    descriptors, read_timings = time_repeat(timed_read, repeats=repeats)

    if cleanup:
        shutil.rmtree(folder)

    return (
        descriptors[0],
        {"run": run_timings, "read": read_timings, "prep": prep_timings},
    )
