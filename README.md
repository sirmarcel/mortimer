# mortimer 🎩⏰

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/python/black) 

`mortimer` is an experimental [`cmlkit`](https://marcel.science/cmlkit) plugin to run detailed timings of representations and kernel computations, developed for the [`repbench`](https://marcel.science/repbench) project. It is deeply integrated with `cmlkit` to provide detailed timings of intermediate steps for representations that need pre- and post-processing steps. `mortimer` also automatically includes platform information in its output.

The functionality is provided as `cmlkit` `Evaluators`.

Supported:
- `SOAP`, `SF`, `MBTR` implemented in `cmlkit`
- `KernelAtomic` and `KernelGlobal` from `cmlkit`
- Representations in [`cscribe`](https://github.com/sirmarcel/cscribe)

Please refer to the code for detailed documentation.

**This is experimental software, please inspect it in detail before relying on its results. It is currently not in active development.**

## Installation

`mortimer` is not yet published on PyPI.

So simply:

- Clone this repository (or add it to your `pyproject.toml`)
- Install requirements (if you have `cmlkit` installed, this is just `py-cpuinfo`)
- Add to `PYTHONPATH`
- Add `mortimer` to your list of `CML_PLUGINS`

If using `poetry` as dependency management system, just add the repository.
